#!/bin/bash

# variables

# directory to save backups to
backup=/home/lily/mnt/backup
# directory to save changed versions of files
versioning=/home/lily/mnt/data/other/versioning
# number of days after which versioning files will be deleted
versioning_timespan=13
# timestamp for versioning folders; has to be a format recognisable by `date` to later convert to unix epoch seconds for age difference calculations, otherwise deleting old versioning folders won't work
timestamp=$(date +%FT%T)
# rsync arguments (--backup means versioning, $@ lets you pass arguments to rsync while starting the script like `backups -v --dry-run`, the excludes are for files moved to the trash can that would otherwise be copied too)
args="-a --delete --inplace --backup --backup-dir $versioning/$timestamp --exclude .Trash-* --exclude lost+found $@"

# --------------------------------------------------------------

# backups

# example
# rsync $args /home/lily/ $backup/home/lily/

rsync $args --exclude "mnt" --exclude ".local/share" --exclude ".steam" --exclude ".cache" --exclude "tmp" --exclude ".config/Element" --exclude ".var/app/com.github.vladimiry.ElectronMail" --exclude ".var/app/org.ferdium.Ferdium" --exclude ".mozilla/firefox/*.default-release/storage" --exclude ".config/quodlibet/config"  --exclude ".config/quodlibet/current"  --exclude ".config/quodlibet/queue" --exclude ".config/quodlibet/songs" --exclude ".Genymobile" /home/lily/ $backup/midna/home/

rsync $args --include "editing" --exclude ".old" --exclude "/editing/**/*.mov" --include "editing/**" --include "projectfiles" --include "projectfiles/**" --include "resolve" --include "resolve/database2" --include "resolve/database2/**" --exclude "*" /home/lily/mnt/fideos2/ $backup/midna/fideos2/

rsync $args --exclude "other/versioning*" --exclude "vms" /home/lily/mnt/data/ $backup/midna/data/

# --------------------------------------------------------------

# remove versioning after specified time

# get versioning directories and their timestamps
versioning_dirs=( "$versioning/*" )
for i in ${versioning_dirs[@]}; do
    versioning_timestamps+=( "${i##*/}" )
done

#calculate versioning timestamp ages in days
minuend=$(date -d $timestamp +%s)
day_difference () {
    subtrahend=$(date -d $1 +%s)
    echo $(( ($minuend - $subtrahend) / 86400 ))
}
for i in ${versioning_timestamps[@]}; do
    if [ $(day_difference $i) -gt $versioning_timespan ]; then
	rm -rf $versioning/$i
    fi
done
